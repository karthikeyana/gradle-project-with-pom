package week3.day3;

import org.testng.annotations.DataProvider;

public class DataProviderClass {
	
	@DataProvider(name="fetchData")
	public String[][] dynamicData() {
		String[][] data = new String[2][6];
		data[0][0]="FIS";
		data[0][1]="Karthikeyan";
		data[0][2]="Anand";
		data[0][3]="Kart";
		data[0][4]="Anandan";
		data[0][5]="IT";
		
		data[1][0]="FIS";
		data[1][1]="Karthik";
		data[1][2]="Anand";
		data[1][3]="Karthikeyan";
		data[1][4]="Anandan";
		data[1][5]="IT";
		
		return data;
	}

}
