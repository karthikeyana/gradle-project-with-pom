package week3.day1;

import org.openqa.selenium.chrome.*;
import org.testng.annotations.Test;

import java.io.IOException;
public class EditLead {
	@Test(dependsOnMethods = "week3.day1.cl()")
	public  void el() throws IOException, InterruptedException {
		System.setProperty("webdriver.chrome.driver","./drivers/chromedriver.exe");
		ChromeDriver driver = new ChromeDriver();
		driver.get("http://leaftaps.com/opentaps");
		driver.manage().window().maximize();
		driver.findElementById("username").sendKeys("DemoSalesManager");
		driver.findElementById("password").sendKeys("crmsfa");
		driver.findElementByClassName("decorativeSubmit").click();
		driver.findElementByLinkText("CRM/SFA").click();
		driver.findElementByLinkText("Leads").click();
		driver.findElementByLinkText("Find Leads").click();
		driver.findElementByXPath("(//input[@name = 'firstName'])[3]").sendKeys("Karthik");
		driver.findElementByXPath("(//input [@name = 'lastName'])[3]").sendKeys("A");
		driver.findElementByXPath("((//button[contains(@id,'ext-gen')]))[7]").click();
		Thread.sleep(3000);
		driver.findElementByXPath("(//a[@class='linktext'])[4]").click();
		String title  = driver.getTitle();
		if(title.equals("View Lead | opentaps CRM")) {
			System.out.println(title);
		}
		else {
			System.out.println("Title not matched");
		}
		driver.findElementByXPath("(//a[@class='subMenuButton'])[3]").click();
		//driver.findElementById("createLeadForm_companyName").clear();
		driver.findElementByXPath("(//input[@id= 'updateLeadForm_companyName'])").clear();
		driver.findElementByXPath("(//input[@id= 'updateLeadForm_companyName'])").sendKeys("FIS Chennai");
		driver.findElementByXPath("(//input[@name = 'submitButton'])[1]").click();
		driver.close();
	}
}