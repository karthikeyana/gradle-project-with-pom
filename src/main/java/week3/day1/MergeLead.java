package week3.day1;

import org.openqa.selenium.chrome.*;
import org.testng.annotations.Test;

import java.util.ArrayList;
import java.util.Set;
public class MergeLead {
	@Test
	public void ml() throws InterruptedException {
		System.setProperty("webdriver.chrome.driver","./drivers/chromedriver.exe");
		ChromeDriver driver = new ChromeDriver();
		driver.get("http://leaftaps.com/opentaps");
		driver.manage().window().maximize();
		driver.findElementById("username").sendKeys("DemoSalesManager");
		driver.findElementById("password").sendKeys("crmsfa");
		driver.findElementByClassName("decorativeSubmit").click();
		driver.findElementByLinkText("CRM/SFA").click();
		driver.findElementByLinkText("Leads").click();
		driver.findElementByLinkText("Merge Leads").click();
		driver.findElementByXPath("(//img[contains(@src,'fieldlookup')])[1]").click();
		Thread.sleep(4000);
		Set <String> allWin = driver.getWindowHandles();
		java.util.List<String> li = new ArrayList<String>();
		li.addAll(allWin);
		driver.switchTo().window(li.get(1));
		driver.manage().window().maximize();
		driver.findElementByXPath("(//input[@name='firstName'])").sendKeys("Karthik");
		driver.findElementByXPath("(//button[text()='Find Leads'])").click();
		Thread.sleep(3000);
		driver.findElementByXPath("(//a[@class='linktext'])[1]").click();
		driver.switchTo().window(li.get(0));
		driver.findElementByXPath("(//img[contains(@src,'fieldlookup')])[2]").click();
		li.addAll(allWin);
		driver.switchTo().window(li.get(2));
		driver.findElementByXPath("(//input[@name='firstName'])").sendKeys("Karthikeyan");
		driver.findElementByXPath("(//button[text()='Find Leads']").click();
		Thread.sleep(3000);
		driver.findElementByXPath("(//a[@class='linktext'])[1]").click();
		driver.switchTo().window(li.get(0));
		driver.findElementByLinkText("Merge").click();
		driver.switchTo().alert();
		String text = driver.switchTo().alert().getText();
		System.out.println(text);
		driver.switchTo().alert().accept();
		driver.findElementByLinkText("Find Leads").click();
		driver.quit();
	}
}
