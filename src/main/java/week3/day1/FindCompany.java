package week3.day1;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.chrome.*;
import org.testng.annotations.Test;

import java.io.File;
import java.io.IOException;
public class FindCompany {
	@Test
	public void companyName() throws IOException, InterruptedException {
		System.setProperty("webdriver.chrome.driver","./drivers/chromedriver.exe");
		ChromeDriver driver = new ChromeDriver();
		driver.get("http://leaftaps.com/opentaps");
		driver.manage().window().maximize();
		driver.findElementById("username").sendKeys("DemoSalesManager");
		driver.findElementById("password").sendKeys("crmsfa");
		driver.findElementByClassName("decorativeSubmit").click();
		driver.findElementByLinkText("CRM/SFA").click();
		driver.findElementByLinkText("Leads").click();
		driver.findElementByLinkText("Find Leads").click();
		driver.findElementByXPath("(//input[@name = 'firstName'])[3]").sendKeys("Karthik");
		driver.findElementByXPath("(//input [@name = 'lastName'])[3]").sendKeys("A");
		driver.findElementByXPath("((//button[contains(@id,'ext-gen')]))[7]").click();
		Thread.sleep(3000);
		driver.findElementByXPath("(//a[@class='linktext'])[13]").click();
		File src = driver.getScreenshotAs(OutputType.FILE);
		File dec = new File ("./snaps/img.jpg");
		FileUtils.copyFile(src, dec);
		driver.close();
	}
}