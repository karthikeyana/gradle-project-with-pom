package week4.day4;

import java.util.HashMap;
import java.util.Map;

public class LearnMap {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		String text = "testleaf";
		char[] ch = text.toCharArray();
		Map <Character, Integer> map = new HashMap<Character, Integer>();
		/*for (char c : ch) {
			System.out.println(c);
		}*/
	
		System.out.println(map.keySet());
		for (char c : ch) {
			System.out.println(c);
			if(map.containsKey(c)) {
				map.put(c, map.get(c)+1);
			}
			else {
				map.put(c, 1);
			}
		}
		System.out.println(map);
	}
}
