package week4.day1;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;

public class LearnReport {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		//ClassName obj = new ClassName();
		ExtentHtmlReporter html = new ExtentHtmlReporter("./Reports/results.html");
		html.setAppendExisting(true);
		ExtentReports extent = new ExtentReports();
		//attach report
		extent.attachReporter(html);
		ExtentTest test = extent.createTest("TC01_Login","Login");
		test.assignAuthor("Karthikeyan");
		test.assignCategory("Functional testing");
		test.pass("Enter the username");
		test.pass("Enter the password");
		test.pass("logged in to the application");
		
		test.fail("login failed");
		//generate report
		extent.flush();
		
	}

}
