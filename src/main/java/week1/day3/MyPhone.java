package week1.day3;

public class MyPhone {

	public static void main(String[] args) {
		Samsung sam = new Samsung();
		System.out.println(sam.dialcaller());
		System.out.println(sam.model());
		System.out.println(sam.name());
		Jio j = new Jio();
		System.out.println(j.dialcaller());
		System.out.println(j.model());
		System.out.println(j.name());
		System.out.println(j.number());
	}

}