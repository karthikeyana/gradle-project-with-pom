package week1.day1;

public class Television {
	public static int channelnumber;
	public int volume = 25;
	public String brandName = "Apple";
	public static int lastChannel = 10;
	public static int firstChannel = 1;
	public int currentVolume() {
		return volume;
	}
	public int currentChannel() {
		return channelnumber;
	}
	public String name() {
		return brandName;
	}
	public static void channelUp() {
		if(channelnumber == lastChannel) {
			channelnumber = firstChannel;
		}
		else {
			channelnumber++;
		}
	}
	public static void channelDown() {
		if(channelnumber == firstChannel) {
			channelnumber = lastChannel;
		}
		else {
			channelnumber--;
		}
	}
}
