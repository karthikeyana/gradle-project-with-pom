package week1.day1;

public class MyPhone {

	public static void main(String[] args) {
		MobilePhone phone = new MobilePhone();
		int phoneCaller = phone.dialCaller();
		System.out.println(phoneCaller);
        String name = phone.name();
        System.out.println(name);
		String model = phone.model();
		System.out.println(model);
		boolean msg = phone.isSent();
		System.out.println(msg);
	}

}
