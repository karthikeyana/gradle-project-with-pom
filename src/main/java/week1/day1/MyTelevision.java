package week1.day1;

public class MyTelevision {

	public static void main(String[] args) {
		Television.channelUp();
		Television.channelDown();
		Television tv = new Television();
		int vol = tv.currentVolume();
		System.out.println(vol);
		int chan = tv.currentChannel();
		System.out.println(chan);
	}
}
