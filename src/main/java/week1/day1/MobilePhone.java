package week1.day1;

public class MobilePhone {
	public int dialCaller() {
		return 978927;
	}
	public String name() {
		return "Samsung";
	}
	public String model() {
		return "J5-2016";
	}
	public boolean isSent() {
		return "true" != null;
	}
}
