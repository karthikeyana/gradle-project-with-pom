package assignments;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.chrome.*;
import java.io.File;
import java.io.IOException;
public class DuplicateLead {
	public static void main(String args[]) throws IOException, InterruptedException {
		System.setProperty("webdriver.chrome.driver","./drivers/chromedriver.exe");
		ChromeDriver driver = new ChromeDriver();
		driver.get("http://leaftaps.com/opentaps");
		driver.manage().window().maximize();
		driver.findElementById("username").sendKeys("DemoSalesManager");
		driver.findElementById("password").sendKeys("crmsfa");
		driver.findElementByClassName("decorativeSubmit").click();
		driver.findElementByLinkText("CRM/SFA").click();
		driver.findElementByLinkText("Leads").click();
		driver.findElementByLinkText("Find Leads").click();
		driver.findElementByXPath("(//a[@class='x-tab-right'])[3]").click();
		driver.findElementByXPath("(//input[@name= 'emailAddress'])").sendKeys("abc@testleaf.com");
		driver.findElementByXPath("(//button[contains(@class,'x-btn-text')])[7]").click();
		File sr = driver.getScreenshotAs(OutputType.FILE);
		File de = new File ("./snaps/img.jpg");
		FileUtils.copyFile(sr, de);
		Thread.sleep(3000);
		driver.findElementByXPath("(//a[@class='linktext'])[4]").click();
		driver.findElementByXPath("(//a[@class='subMenuButton'])[1]").click();
		driver.findElementByXPath("(//a[@class='linktext'])[4]").click();
		String title  = driver.getTitle();
		if(title.equals("Duplicate Lead | opentaps CRM")) {
			System.out.println(title);
		}
		else {
			System.out.println("Title not matched");
		}
		driver.findElementByXPath("(//input[@class='smallSubmit'])").click();
		driver.findElementByLinkText("Find Leads").click();
		driver.findElementByXPath("(//input[@name= 'firstName'])[3]").sendKeys("Karthik");
		driver.findElementByXPath("(//input[@name= 'lastName'])[3]").sendKeys("A");
		driver.findElementByXPath("((//button[contains(@id,'ext-gen')]))[7]").click();
		File sr2 = driver.getScreenshotAs(OutputType.FILE);
		File de2 = new File ("./snaps/img.jpg");
		FileUtils.copyFile(sr2, de2);
		driver.close();
	}
}
