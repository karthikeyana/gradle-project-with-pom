package assignments;

import org.openqa.selenium.chrome.*;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.WebElement;
import java.io.File;
import java.io.IOException;
import org.apache.commons.io.FileUtils;
import org.openqa.selenium.support.ui.Select;
public class CreateLead {
	
	public static void main(String args[]) throws IOException {
		System.setProperty("webdriver.chrome.driver","./drivers/chromedriver.exe");
		ChromeDriver driver = new ChromeDriver();
		driver.get("http://leaftaps.com/opentaps");
		driver.manage().window().maximize();
		driver.findElementById("username").sendKeys("DemoSalesManager");
		driver.findElementById("password").sendKeys("crmsfa");
		driver.findElementByClassName("decorativeSubmit").click();
		driver.findElementByLinkText("CRM/SFA").click();
		driver.findElementByLinkText("Leads").click();
		driver.findElementByLinkText("Create Lead").click();
		driver.findElementById("createLeadForm_companyName").sendKeys("FIS");
		driver.findElementById("createLeadForm_firstName").sendKeys("Karthikeyan");
		driver.findElementById("createLeadForm_lastName").sendKeys("A");
		driver.findElementById("createLeadForm_firstNameLocal").sendKeys("Karthik");
		driver.findElementById("createLeadForm_lastNameLocal").sendKeys("Keyan");
		driver.findElementById("createLeadForm_generalProfTitle").sendKeys("IT");
		driver.findElementById("createLeadForm_departmentName").sendKeys("Payments");
		driver.findElementById("createLeadForm_annualRevenue").sendKeys("30000");
		driver.findElementById("createLeadForm_numberEmployees").sendKeys("100");
		driver.findElementById("createLeadForm_description").sendKeys("create lead form");
		driver.findElementById("createLeadForm_importantNote").sendKeys("ImportantNotes");
		driver.findElementById("createLeadForm_primaryPhoneAreaCode").sendKeys("201");
		driver.findElementById("createLeadForm_primaryPhoneExtension").sendKeys("+91");
		driver.findElementById("createLeadForm_primaryEmail").sendKeys("abc@testleaf.com");
		driver.findElementById("createLeadForm_primaryWebUrl").sendKeys("www.google.com");
		driver.findElementById("createLeadForm_generalToName").sendKeys("Anand");
		WebElement state = driver.findElementById("createLeadForm_generalStateProvinceGeoId");
		Select st = new Select (state);
		st.selectByVisibleText("Illinois");
		driver.findElementById("createLeadForm_generalAddress1").sendKeys("Arcot");
		driver.findElementById("createLeadForm_generalAddress2").sendKeys("U.S");
		driver.findElementById("createLeadForm_generalCity").sendKeys("Chennai");
		driver.findElementById("createLeadForm_generalPostalCode").sendKeys("600042");
		WebElement eleSource = driver.findElementById("createLeadForm_dataSourceId");
		Select sc = new Select (eleSource);
		sc.selectByVisibleText("Public Relations");
		WebElement mcSource = driver.findElementById("createLeadForm_marketingCampaignId");
		Select mc = new Select (mcSource);
		mc.selectByVisibleText("Automobile");
		WebElement industry = driver.findElementById("createLeadForm_industryEnumId");
		Select in = new Select (industry);
		in.selectByVisibleText("Computer Software");
		driver.findElementByName("submitButton").click();
		File src = driver.getScreenshotAs(OutputType.FILE);
		File dec = new File ("./snaps/img.jpg");
		FileUtils.copyFile(src, dec);
		driver.close();
	}
}