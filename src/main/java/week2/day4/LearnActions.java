package week2.day4;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.remote.RemoteWebDriver;

public class LearnActions {

	public static void main(String[] args) throws InterruptedException {
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		RemoteWebDriver driver = new ChromeDriver();
		driver.manage().window().maximize();
		driver.get("http://jqueryui.com/draggable/");
		driver.switchTo().frame(0);
		WebElement drag = driver.findElementById("draggable");
		int x = driver.findElementById("draggable").getLocation().getX();
		int y = driver.findElementById("draggable").getLocation().getY();
		Actions builder = new Actions(driver);
		builder.dragAndDropBy(drag, x+100,y+100).perform();
	}

}
