package week2.day2;

import java.util.ArrayList;
import java.util.Set;

import org.openqa.selenium.chrome.ChromeDriver;
public class WindowCalling {
	public static void main(String[] args) {
			System.setProperty("webdriver.chrome.driver","./drivers/chromedriver.exe");
			ChromeDriver driver = new ChromeDriver();
			driver.get("https://www.irctc.co.in/eticketing/userSignUp.jsf");
			driver.manage().window().maximize();
			System.out.println(driver.getTitle());
			driver.findElementByLinkText("Contact Us").click();
			Set <String> allWin = driver.getWindowHandles();
			java.util.List<String> li = new ArrayList<String>();
			li.addAll(allWin);
			driver.switchTo().window(li.get(1));
			driver.manage().window().maximize();
			System.out.println(driver.getTitle());
			/*driver.switchTo().window(li.get(0));
			driver.close();*/
			driver.findElementByLinkText("Home").click();
	}
}
