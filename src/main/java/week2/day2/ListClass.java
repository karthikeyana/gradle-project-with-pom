package week2.day2;

import java.util.ArrayList;
import java.util.List;

public class ListClass {

	public static void main(String[] args) {
		LearnList();
	}
	public static void LearnList() {
		List<String> list = new ArrayList<String> ();
		list.add("Karthik");
		list.add("Balaji");
		list.add("Prasanna");
		list.add("viji");
		list.add("Dinesh");
		System.out.println(list.size());
		System.out.println(list.get(4));
		System.out.println(list.contains("viji"));
		for (String li : list) {
			System.out.println(li);
		}
		list.clear();
		System.out.println(list.isEmpty());
		list.add("uma");
		list.add("valli");
		list.add("lachu");
		list.add("tinku");
		list.add("papu");
		if(list.contains("pa")) {
			System.out.println("True");
		}
		else {
			System.out.println("Not in the list");
		}
			
	}
}
