package week2.day5;


import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.remote.RemoteWebDriver;

public class ZoomCar {

	public static void main(String[] args) throws InterruptedException {
		// TODO Auto-generated method stub

		System.setProperty("webdriver.chrome.driver", "./Drivers/chromedriver.exe");
		RemoteWebDriver driver = new ChromeDriver();
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		driver.get("https://www.zoomcar.com/chennai");
		driver.findElementByLinkText("Start your wonderful journey").click();
		driver.findElementByXPath("(//div[@class='items'])[3]").click();
		driver.findElementByClassName("proceed").click();
		/*driver.findElementByXPath("(//div[@class='text'])[2]").click();
			driver.findElementByClassName("proceed").click();
		 */
		// Get the current date
		Date date = new Date();
		// Get only the date (and not month, year, time etc)
		DateFormat sdf = new SimpleDateFormat("dd");
		// Get today's date
		String today = sdf.format(date);
		// Convert to integer and add 1 to it
		int tomorrow = Integer.parseInt(today)+1;
		// Print tomorrow's date
		System.out.println(tomorrow);

		driver.findElementByXPath("//div[contains(text(), '"+tomorrow+"')]").click(); 
		driver.findElementByClassName("proceed").click();
		driver.findElementByClassName("proceed").click();
		Thread.sleep(2000);

		List<Integer> alllist =new ArrayList<Integer>();


		List<WebElement> listprice = driver.findElementsByXPath("//div[@class='price']");
		for (WebElement price : listprice) {
			String lists = price.getText();
			String place = lists.replaceAll("\\D", "");
			int as = Integer.parseInt(place);

			alllist.add(as);
			System.out.println(as);

		}

		Integer max = Collections.max(alllist);
		System.out.println(max);

		String model = driver.findElementByXPath("(//div[contains(text(),'"+max+"')])[1]/preceding::h3[1]").getText();

		System.out.println(model);


	}

}

