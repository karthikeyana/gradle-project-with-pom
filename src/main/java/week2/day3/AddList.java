package week2.day3;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class AddList {

	public static void main(String[] args) {
		List<String> list = new ArrayList<String>();
		list.add("Karthik");
		list.add("Balaji");
		list.add("Viji");
		list.add("Dinesh");
		list.add("Dinesh");
		System.out.println("The below list is:" +list.size());
		System.out.println(list.remove(2));
		System.out.println(list.size());
		System.out.println(list.remove("Karthik"));
		System.out.println(list.size());
		for (String eachList : list) {
			System.out.println(eachList);
		}
		list.add("Karthik");
		list.add("Balaji");
		list.add("Viji");
		list.add("Dinesh");
		list.add("Dinesh");
		list.add("Dinesh");
		Set<String> set = new HashSet<>();
		set.addAll(list);
		System.out.println(list);
		System.out.println(set);
	}

}
