package week2.day3;

import java.util.HashSet;
import java.util.Set;

public class AddSet {

	public static void main(String[] args) {
		Set<String> set = new HashSet<String>();
		set.add("Karthik");
		set.add("Viji");
		set.add("Balaji");
		set.add("Dinesh");
		set.add("Karthik");
		System.out.println(set.size());
		System.out.println(set.remove("Karthik"));
		System.out.println(set.size());
		for (String setList : set) {
			System.out.println(setList);
		}
		set.add("Karthik");
		System.out.println(set.size());
		for (String setLi : set) {
			System.out.println(setLi);
		}
		
	}
}
