package testcases;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;
import org.testng.annotations.BeforeMethod;
import week3.day1.ProjSpecificMethods;

public class CreateLeadAnnotations extends ProjSpecificMethods {
	
	@BeforeMethod
	public void createLead() {
		WebElement eleCl = locateElement("linkText","Create Lead");
		click(eleCl);
		WebElement eleCn = locateElement("id", "createLeadForm_companyName");
		type(eleCn,"FIS");
		WebElement eleFn = locateElement("id", "createLeadForm_firstName");
		type(eleFn,"Karthikeyan");
		WebElement eleLn = locateElement("id", "createLeadForm_lastName");
		type(eleLn,"Anand");
		WebElement eleFnL = locateElement("id", "createLeadForm_firstNameLocal");
		type(eleFnL,"Kart");
		WebElement eleLnL = locateElement("id", "createLeadForm_lastNameLocal");
		type(eleLnL,"Anandan");
		WebElement eleGp = locateElement("id", "createLeadForm_generalProfTitle");
		type(eleGp, "IT");
		WebElement eleRevenue = locateElement ("id", "createLeadForm_annualRevenue");
		type(eleRevenue, "30000");
		WebElement eleNumEmp = locateElement ("id", "createLeadForm_numberEmployees");
		type(eleNumEmp, "200");
		WebElement eleForm = locateElement("id", "createLeadForm_description");
		type(eleForm, "Create Lead Form");
		WebElement eleImpNotes = locateElement("id", "createLeadForm_importantNote");
		type(eleImpNotes, "Notes");
		WebElement elePhCode = locateElement("id", "createLeadForm_primaryPhoneAreaCode");
		type(elePhCode, "201");
		WebElement eleExtension = locateElement("id", "createLeadForm_primaryPhoneExtension");
		type(eleExtension, "+91");
		WebElement eleMail = locateElement("id", "createLeadForm_primaryEmail");
		type(eleMail, "karthik@KI.com");
		WebElement eleUrl = locateElement("id", "createLeadForm_primaryWebUrl");
		type(eleUrl, "www.karthikblog.com");
		WebElement eleDropDown = locateElement("id", "createLeadForm_generalStateProvinceGeoId");
		Select eleDd = new Select (eleDropDown);
		eleDd.selectByVisibleText("Illinois");
		WebElement eleClick = locateElement ("class", "smallSubmit");
		click(eleClick);
	}
	
}
