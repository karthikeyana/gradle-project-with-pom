package testcases;


import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.testng.annotations.Test;

import wdmethods.SeMethods;

public class Tc003_EditLead extends SeMethods {
	
	ChromeDriver driver;
	Actions builder;
	@Test
	public void editLead() throws InterruptedException {
		startApp("chrome", "http://leaftaps.com/opentaps");
		WebElement eleUsername = locateElement("id", "username");
		type(eleUsername, "DemoSalesManager");
		WebElement elePassword = locateElement("id", "password");
		type(elePassword, "crmsfa");
		WebElement eleLogin = locateElement("class", "decorativeSubmit");
		click(eleLogin);
		WebElement eleCrmsfa = locateElement("linkText", "CRM/SFA");
		click(eleCrmsfa);
		WebElement eleLeads = locateElement("linkText", "Leads");
		click(eleLeads);
		WebElement eleFl = locateElement("linkText","Find Leads");
		click(eleFl);
		WebElement eleFiN = locateElement("xpath", "(//input[@name = 'firstName'])[3]");
		type(eleFiN, "Karthikeyan");
		WebElement eleLaN = locateElement("xpath", "(//input[@name = 'lastName'])[3]");
		type(eleLaN, "Anand");
		WebElement eleFindB = locateElement("xpath", "((//button[contains(@id,'ext-gen')]))[7]");
		click(eleFindB);
		Thread.sleep(2000);
		WebElement eleFind = locateElement("xpath", "(//a[@class='linktext'])[4]");
		click(eleFind);
		WebElement eleEdit = locateElement("xpath", "(//a[@class='subMenuButton'])[3]");
		click(eleEdit);
		WebElement eleCompN = locateElement("xpath", "(//input[@id= 'updateLeadForm_companyName])");
		eleCompN.clear();
		type(eleCompN, "FIS Payments");
		WebElement eleClick = locateElement ("xpath", "//input[@name = 'submitButton'])[1]");
		click(eleClick);
		closeAllBrowsers();
	}
}
