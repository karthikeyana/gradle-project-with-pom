package testcases;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.testng.annotations.Test;

import wdmethods.SeMethods;

public class Tc005_DeleteLead extends SeMethods {
	ChromeDriver driver;
	Actions builder;
	@Test
	public void deleteLead() throws InterruptedException {
		startApp("chrome", "http://leaftaps.com/opentaps");
		WebElement eleUsername = locateElement("id", "username");
		type(eleUsername, "DemoSalesManager");
		WebElement elePassword = locateElement("id", "password");
		type(elePassword, "crmsfa");
		WebElement eleLogin = locateElement("class", "decorativeSubmit");
		click(eleLogin);
		WebElement eleCrmsfa = locateElement("linkText", "CRM/SFA");
		click(eleCrmsfa);
		WebElement eleLeads = locateElement("linkText", "Leads");
		click(eleLeads);
		WebElement eleFl = locateElement("linkText","Find Leads");
		click(eleFl);
		WebElement eleFiN = locateElement("xpath", "(//input[@name = 'firstName'])[3]");
		type(eleFiN, "Karthikeyan");
		WebElement eleLaN = locateElement("xpath", "(//input[@name = 'lastName'])[3]");
		type(eleLaN, "Anand");
		WebElement eleFindB = locateElement("xpath", "((//button[contains(@id,'ext-gen')]))[7]");
		click(eleFindB);
		Thread.sleep(2000);
		WebElement eleFind = locateElement("xpath", "(//a[@class='linktext'])[4]");
		click(eleFind);
		WebElement eleDel = locateElement("xpath", "(//a[@class= 'subMenuButtonDangerous'])");
		click(eleDel);
		WebElement eleLs = locateElement("linkText","Find Leads");
		click(eleLs);
		WebElement eleFi3 = locateElement("xpath", "(//input[@name = 'firstName'])[3]");
		type(eleFi3, "Karthikeyan");
		WebElement eleLa3 = locateElement("xpath", "(//input[@name = 'lastName'])[3]");
		type(eleLa3, "Anand");
		WebElement eleFindC = locateElement("xpath", "((//button[contains(@id,'ext-gen')]))[7]");
		click(eleFindC);
	}
}
