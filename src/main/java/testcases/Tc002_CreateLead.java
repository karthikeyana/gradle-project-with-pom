package testcases;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;
import org.testng.annotations.Test;

import wdmethods.SeMethods;


public class Tc002_CreateLead extends SeMethods {
	
	@Test(groups = "smoke")
	public void CreateLead() {
		
		startApp("chrome", "http://leaftaps.com/opentaps");
		WebElement eleUsername = locateElement("id", "username");
		type(eleUsername, "DemoSalesManager");
		WebElement elePassword = locateElement("id", "password");
		type(elePassword, "crmsfa");
		WebElement eleLogin = locateElement("class", "decorativeSubmit");
		click(eleLogin);
		WebElement eleCrmsfa = locateElement("linkText", "CRM/SFA");
		click(eleCrmsfa);
		WebElement eleLeads = locateElement("linkText", "Leads");
		click(eleLeads);
		WebElement eleCl = locateElement("linkText","Create Lead");
		click(eleCl);
		WebElement eleCn = locateElement("id", "createLeadForm_companyName");
		type(eleCn,"FIS");
		WebElement eleFn = locateElement("id", "createLeadForm_firstName");
		type(eleFn,"Karthikeyan");
		WebElement eleLn = locateElement("id", "createLeadForm_lastName");
		type(eleLn,"Anand");
		WebElement eleFnL = locateElement("id", "createLeadForm_firstNameLocal");
		type(eleFnL,"Kart");
		WebElement eleLnL = locateElement("id", "createLeadForm_lastNameLocal");
		type(eleLnL,"Anandan");
		WebElement eleGp = locateElement("id", "createLeadForm_generalProfTitle");
		type(eleGp, "IT");
		WebElement eleRevenue = locateElement ("id", "createLeadForm_annualRevenue");
		type(eleRevenue, "30000");
		WebElement eleNumEmp = locateElement ("id", "createLeadForm_numberEmployees");
		type(eleNumEmp, "200");
		WebElement eleForm = locateElement("id", "createLeadForm_description");
		type(eleForm, "Create Lead Form");
		WebElement eleImpNotes = locateElement("id", "createLeadForm_importantNote");
		type(eleImpNotes, "Notes");
		WebElement elePhCode = locateElement("id", "createLeadForm_primaryPhoneAreaCode");
		type(elePhCode, "201");
		WebElement eleExtension = locateElement("id", "createLeadForm_primaryPhoneExtension");
		type(eleExtension, "+91");
		WebElement eleMail = locateElement("id", "createLeadForm_primaryEmail");
		type(eleMail, "karthik@KI.com");
		WebElement eleUrl = locateElement("id", "createLeadForm_primaryWebUrl");
		type(eleUrl, "www.karthikblog.com");
		WebElement eleDropDown = locateElement("id", "createLeadForm_generalStateProvinceGeoId");
		Select eleDd = new Select (eleDropDown);
		eleDd.selectByVisibleText("Illinois");
		WebElement eleClick = locateElement ("class", "smallSubmit");
		click(eleClick);
	}
}
