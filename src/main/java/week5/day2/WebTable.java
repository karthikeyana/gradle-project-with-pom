package week5.day2;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class WebTable {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		System.setProperty("webdriver.chrome.driver","./drivers/chromedriver.exe");
		ChromeDriver driver = new ChromeDriver();
		driver.get("http://leafground.com/pages/table.html");
		driver.manage().window().maximize();
		WebElement table = driver.findElementByTagName("table");
		List<WebElement> rows = table.findElements(By.tagName("tr"));
		System.out.println(rows.size());
		for (int i = 1; i < rows.size(); i++) {
			List<WebElement> text = rows.get(i).findElements(By.tagName("td"));
			if(text.get(1).getText().equals("80%")) {
				text.get(2).click();
				break;
			}
		}
	}
}
