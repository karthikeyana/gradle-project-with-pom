package week5.day2;

public class LearnString {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		//1. toCharArray()
		String text = "Karthikeyan";
		System.out.println(text);
		char[] ch = text.toCharArray();
		for (char c : ch) {
			System.out.println(c);
		}

		//2. reverse:
		StringBuffer text1 = new StringBuffer("Karthikeyan");
		System.out.println(text1.reverse());

		//3. reverse in other method
		String text2 = "Karthikeyan";
		System.out.println(text2);
		//char[] ch1 = text2.toCharArray();
		for (int i = text2.length()-1; i >=0 ; i--) {
			char text3 = text.charAt(i);
			System.out.println(text3);
		}

		//4. reverse without array:
		String text4 = "Karthikeyan";
		System.out.println(text4.length());
		for (int i = text4.length()-1; i >=0; i--) {
			char text5 = text.charAt(i);
			System.out.println(text5);
		}

		//5. In a given string a = "hi pls find the in between characters" find all the characters between first
		//occurence of 's to last occurence of 's'

		String a = "hi pls find the in between characters";
		String substring = a.substring(a.indexOf("s")+1, a.lastIndexOf("s")-1);
		System.out.println(substring.trim());
		System.out.println(substring.length());
		/*		System.out.println(a.length());
		System.out.println(a.indexOf("s"));
		System.out.println(a.lastIndexOf("s"));
		System.out.println(a.substring(5,36));
		 */
		//6. Variable "Amazon India"
		String str = " Amazon India";
		char [] array = str.toCharArray();
		int count = 0;
		for (char c : array) {
			if (c== 'A' || c == 'a') {
				count++;
			}
		}
		System.out.println(count);
	}
}
