package wdmethods;

import org.testng.annotations.Test;

import wdmethods.SeMethods;

import org.testng.annotations.BeforeMethod;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Parameters;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.AfterSuite;

public class ProjSpecificMethods extends SeMethods {
	public ChromeDriver driver;
	public void f() {
		System.out.println("LearnAnnotations");
	}
	@BeforeMethod
	public void beforeMethod() {
		startApp("chrome", "http://leaftaps.com/opentaps");
		WebElement eleUsername = locateElement("id", "username");
		type(eleUsername, "DemoSalesManager");
		WebElement elePassword = locateElement("id", "password");
		type(elePassword, "crmsfa");
		WebElement eleLogin = locateElement("class", "decorativeSubmit");
		click(eleLogin);
		WebElement eleCrmsfa = locateElement("linkText", "CRM/SFA");
		click(eleCrmsfa);
		WebElement eleLeads = locateElement("linkText", "Leads");
		click(eleLeads);
		WebElement eleCl = locateElement("linkText","Create Lead");
		click(eleCl);
		
	}

	@AfterMethod
	public void afterMethod() {
		closeAllBrowsers();
	}

	@BeforeClass
	public void beforeClass() {
		System.out.println("BeforeClass");
	}

	@AfterClass
	public void afterClass() {
		System.out.println("AfterClass");
	}

	@BeforeTest
	public void beforeTest() {
		System.out.println("before test");
	}

	@AfterTest
	public void afterTest() {
		System.out.println("After test");
	}

	@BeforeSuite
	public void beforeSuite() {
		System.out.println("beforeSuite");
	}

	@AfterSuite
	public void afterSuite() {
		System.out.println("AferSuite");
	}

}
