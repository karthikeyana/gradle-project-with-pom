package wdmethods;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.Select;

public class SeMethods implements WdMethods {

	RemoteWebDriver driver = null;
	@Override
	public void startApp(String browser, String url) {

		if (browser.equalsIgnoreCase("chrome")) {
			System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
			driver = new ChromeDriver();
		} else if(browser.equalsIgnoreCase("firefox")) {
			System.setProperty("webdriver.gecko.driver", "./drivers/geckodriver.exe");
			driver = new FirefoxDriver();
		}
		driver.get(url);
		driver.manage().window().maximize();
		System.out.println("The browser "+browser+" launched successfully");
	}

	@Override
	public WebElement locateElement(String locator, String locValue) {
		try {
			switch (locator) {
			case "id": return driver.findElementById(locValue); 
			case "class": return  driver.findElementByClassName(locValue);
			case "tagName": return driver.findElementByTagName(locValue);
			case "linkText": return driver.findElementByLinkText(locValue);
			case "xpath" : return driver.findElementByXPath(locValue);
			case "cssSelector" : return driver.findElementByCssSelector(locValue);
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}

	@Override
	public WebElement locateElement(String locValue) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void type(WebElement ele, String data) {
		try {
			ele.clear();
			ele.sendKeys(data);	
			System.out.println("The data "+data+" entered successfully");
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Override
	public void click(WebElement ele) {
		ele.click();
		System.out.println("The element "+ele+ "click successfully");
	}

	@Override
	public String getText(WebElement ele) {
		String text=ele.getText();
		System.out.println("The element clicked successfully" +ele);
		return text;
		// TODO Auto-generated method stub

	}

	@Override
	public void selectDropDownUsingText(WebElement ele, String value) {
		Select sel = new Select(ele);
		sel.selectByVisibleText(value);
		System.out.println("The value "+value+" entered successfully");
	}

	@Override
	public void selectDropDownUsingIndex(WebElement ele, int index) {
		Select sel = new Select (ele);
		sel.selectByIndex(index);
		System.out.println("The value is:"+index);
	}

	@Override
	public boolean verifyTitle(String expectedTitle) {
		String actualTitle = driver.getTitle();
		if(actualTitle.equals(expectedTitle)) {
			return true;
		}
		else {
			return false;
		}
	}

	@Override
	public void verifyExactText(WebElement ele, String expectedText) {
		// TODO Auto-generated method stub
		String text=ele.getText();
		if(text.equals(expectedText)) {
			System.out.println("Text displayed correctly" +ele);
		}
		else {
			System.out.println("Text is not displayed correctly");
		}

	}

	@Override
	public void verifyPartialText(WebElement ele, String expectedText) {
		// TODO Auto-generated method stub
		String text=ele.getText();
		if(text.contains(expectedText)) {
			System.out.println("Text displayed correctly" +ele);
		}
		else {
			System.out.println("Text is not displayed correctly");
		}


	}

	@Override
	public void verifyExactAttribute(WebElement ele, String attribute, String value) {
		// TODO Auto-generated method stub
		String attribute1 = ele.getAttribute(attribute);
		if(attribute1.equalsIgnoreCase(value)) {
			System.out.println("Attribute displayed succesfully" +attribute1);
		}
		else {
			System.out.println("attribute not displayed");
		}

	}

	@Override
	public void verifyPartialAttribute(WebElement ele, String attribute, String value) {
		// TODO Auto-generated method stub
		String attribute2 = ele.getAttribute(attribute);
		if(attribute2.contains(value)) {
			System.out.println("Attribute for partial text displayed successfully" +attribute2);
		}
		else {
			System.out.println("attribute not displayed");
		}

	}

	@Override
	public void verifySelected(WebElement ele) {
		// TODO Auto-generated method stub
		if(ele.isSelected()) {
			System.out.println("Element is Selected" +ele );
		}
		else {
			System.out.println("Element is not selected");
		}

	}

	@Override
	public void verifyDisplayed(WebElement ele) {
		// TODO Auto-generated method stub
		if(ele.isDisplayed()) {
			System.out.println("Element is displayed" +ele);
		}
		else {
			System.out.println("Element is not displayed");
		}

	}

	@Override
	public void switchToWindow(int index) {
		// TODO Auto-generated method stub
		Set<String> allWindows = driver.getWindowHandles();
		List<String> window = new ArrayList<String>();
		window.addAll(allWindows);
		driver.switchTo().window(window.get(index));
	}

	@Override
	public void switchToFrame(WebElement ele) {
		// TODO Auto-generated method stub
		driver.switchTo().frame(ele);

	}

	@Override
	public void acceptAlert() {
		// TODO Auto-generated method stub
		driver.switchTo().alert().accept();

	}

	@Override
	public void dismissAlert() {
		// TODO Auto-generated method stub
		driver.switchTo().alert().dismiss();

	}

	@Override
	public String getAlertText() {
		// TODO Auto-generated method stub
		String text = driver.switchTo().alert().getText();
		return text;
	}

	@Override
	public void takeSnap() {
		// TODO Auto-generated method stub
		File src = driver.getScreenshotAs(OutputType.FILE);
		File dec = new File ("./snaps/img.jpg");
		try {
			FileUtils.copyFile(src, dec);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Override
	public void closeBrowser() {
		driver.close();
	}

	@Override
	public void closeAllBrowsers() {
		driver.quit();
	}

}
